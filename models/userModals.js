const mongoose = require("mongoose")
const validator = require('validator')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: [true, 'please tell your name']
    },

    email:{
        type: String,
        required: [true, 'please tell your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'please provide a valid email'],
    },

    photo:{
        type: String,
        default: 'default.jpg'
    },

    role:{
        type: String,
        enum: ['user', 'sme', 'pharmist', 'admin'],
        default: 'user'
    },

    password:{
        type: String,
        required: [true, 'please tell your password'],
        minlength: 8,
        select: false,
    },
    passwordConfirm: {
        type: String,
        required: [true, 'please confirm your password'],
        
        validate: {
            validator: function (el) {
                return el === this.password
            },
            message: "password are not same"
        }
    },
    active:{
        type: Boolean,
        default: true,
        select: false,
    },
    
})


userSchema.methods.correctPassword= async function(
    candidatePassword,
    userPassword,
){
    return await bcrypt.compare(candidatePassword, userPassword)
}

userSchema.pre('save', async function (next){
    if(!this.isModified('password')) return next()

    this.password = await bcrypt.hash(this.password, 12)

    this.passwordConfirm = undefined
    next()

})

const User = mongoose.model('User', userSchema)
module.exports = User






userSchema.pre('save', async function (next){
    const update = this.getUpdate();
    if(update.password !== '' &&
       update.password !== undefined &&
       update.password == update.passwordConfirm){
        // this.getUpdate().password = await bcrypt.hash(update.password, 12)

        update.passwordConfirm = undefined
        next()
    
       }else
       next()
})



