const mongoose = require("mongoose")
const dotenv = require("dotenv")
dotenv.config({path: './config.env'})
const app = require('./app')


const DB = process.env.DATABASE.replace(
    'PASSWORD',
    process.env.DATABASE_PASSWORD
)

const local_DB = process.env.DATABASE_LOCAL

mongoose.connect(local_DB).then((con) =>{
    console.log("DB successful connection")
}).catch(error => console.log(error));

// cloud
// mongoose.connect(DB).then((con)=>{
//     console.log(con.connection)
//     console.log('DB connection successful')
// }).catch(error => console.log(error));

const port = 4001
app.listen(port, () =>{
    console.log(`App running on port ${port} ..`)
})

// const express = require("express")

// const app = express()

module.exports = app     